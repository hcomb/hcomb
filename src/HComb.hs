module HComb
    ( Board
    , solve
    ) where

import Board (Board)
import Solve (solve)