module Solve (solve) where

import Board
import Cell
                
{-|
  Represents a node in a "decision tree", which keeps track of all the arbitrary decisions that have
  been made during solving. Each node in the tree contains a set of alternative board snapshots that
  may be interpreted like: "what would the board look like if, at the point of making the decision,
  an alternative option was chosen?"
-}
data DecisionNode = RootNode { alternatives :: [Board] } |
                    Node     { alternatives :: [Board]
                             , parent       :: DecisionNode } deriving (Show)

{-|
  Tries to solve the given puzzle and return the solution.

  If the game is not solvable, returns Nothing.
-}
solve :: Board -> Maybe Board
solve board =
    -- The algorithm works as follows:
    --   1. Update exclusions in the board (spread them).
    --   2. Scan the board:
    --     2.1. If all fields in the board have exactly 6 exclusions, the puzzle is solved, return the board.
    --     2.2. If there is any field in the board that has more than 6 exclusions, the puzzle is broken.
    --          Go back up the decision tree and remove the failed subtree. If there are no more decisions
    --          in the tree that we may backtrack to, give up - the puzzle is ill-formed.
    --     2.3. If the game is neither solved nor broken, pick some new, underdetermined cell, set its value
    --          and add a new decision node to the tree with all the other acceptable letters as branches.
    --   3. Repeat.
    doSolve (RootNode []) board
    
    where
        {-|
          Keeps performing the algorithm (described above) until a definite answer is reached.
        -}
        doSolve :: DecisionNode -> Board -> Maybe Board
        doSolve curNode b =
            let ub = updateExclusions b        -- Spread exclusions across the board.
            in
                if isSolved ub then            -- If the puzzle is solved, return current state of the board.
                    Just ub
                else if isIllFormed ub then    -- If the board is ill-formed, backtrack, reversing the previous decision
                    case backtrack curNode of  -- and continue solving. If there's no decision to reverse, give up.
                        Just (nextNode, restoredBoard) -> doSolve nextNode restoredBoard
                        Nothing                        -> Nothing
                else                           -- Otherwise try filling some new cell with a value and keep on solving.
                    uncurry doSolve $ fillSomeCell curNode ub

        {-|
          Updates the board state such that there is an "exclusion region" around every letter.

          E.g. if there is a board like below:

                 [ ][ ][ ][ ]
                [ ][ ][ ][ ][ ]
                 [ ][A][ ][ ]
                [ ][ ][ ][ ][ ]
                 [ ][ ][ ][ ]

          Then it's impossible for another 'A' to occur anywhere in it's direct neighborhood
          or in it's neighbors' direct neighborhood without breaking the rules. Therefore
          we form an "exclusion region" around it, disallowing 'A':

                 [-][-][-][ ]
                [-][-][-][-][ ]
                 [-][A][-][-]
                [-][-][-][-][ ]
                 [-][-][-][ ]
        -}
        updateExclusions :: Board -> Board
        updateExclusions (Board arity cells) = Board arity $ doUpdateExclusions 0 cells
            where
                {-|
                  Keeps spreading exclusions around determined fields until we finally run out of them.
                  Returns an updated board with exclusion regions properly determined.

                  Each determined field is used exactly once.
                -}
                doUpdateExclusions :: Int -> BoardState -> BoardState
                doUpdateExclusions skip cs =
                    let unchecked = drop skip cs
                    in 
                        case boardStateFindDeterminedCell unchecked of
                            Just (cell, cellIdx) ->
                                let realIdx = cellIdx + skip in
                                    doUpdateExclusions (realIdx + 1) $ spreadExclusion cell realIdx cs
                            Nothing              -> cs
                    where
                        {-|
                          Excludes the determined value of the given field from all of its neighbors
                          and all of its neighbors' neighbors. Returns the altered board.
                        -}
                        spreadExclusion :: CellState -> Int -> BoardState -> BoardState
                        spreadExclusion cell idx acc =
                            let
                                Just excludedLetter = cellValue cell
                                -- Take all direct neighbors of the field.
                                directNeighbors     = boardGetNeighborIndices arity idx
                                -- And a list of each direct neighbor's neighbors.
                                indirectNeighbors   = map (boardGetNeighborIndices arity) directNeighbors
                                -- Flatten the list and remove duplicates.
                                uniqueNeighbors     = unique [] $ foldr (++) directNeighbors indirectNeighbors
                                -- Finally, remove the field from which the exclusions have originated,
                                -- as it MUST NOT have the value excluded (in fact it's the only value
                                -- that shall not be excluded within the cell).
                                withoutSelf         = filter (/= idx) uniqueNeighbors
                            in
                                foldr (boardStateExclude excludedLetter) acc withoutSelf

                {-|
                  Removes duplicate elements from the given list.
                -}
                unique soFar []    = soFar
                unique soFar (h:t) =
                    if h `elem` soFar then unique soFar t
                    else                   unique (h : soFar) t

        {-|
          Checks whether the puzzle is solved. It is if there are letters in every cell.
        -}
        isSolved :: Board -> Bool
        isSolved (Board _ cells) = all cellDetermined cells

        {-|
          Checks whether the puzzle is ill-formed. It is if it contains any overdetermined
          cells (i.e. cells having all the letters disallowed).
        -}
        isIllFormed :: Board -> Bool
        isIllFormed (Board _ cells) = any cellOverdetermined cells

        {-|
          Tries to go back up the decision tree, reversing the most recent decision and picking
          an alternative instead. If we're already at the top of the tree, it cannot do anything
          more and so returns Nothing.
        -}
        backtrack :: DecisionNode -> Maybe (DecisionNode, Board)
        backtrack (RootNode _) = Nothing               -- We cannot backtrack from root, so give up.
        backtrack (Node _ par) =
            case alternatives par of
                []    -> backtrack par                 -- If there are no alternative decisions at the parent's level,
                                                       -- backtrack again, to the parent's parent.
                (h:t) -> Just                          -- Otherwise pick the next alternative and create a corresponding
                    ( Node [] (withAlternatives t par) -- child decision node, dropping the previous branch.
                    , h)

        {-|
          Finds an arbitrary non-determined field and fills it with some non-excluded value.
          Remembers the decision within the decision tree along with the alternative snapshots.
        -}
        fillSomeCell :: DecisionNode -> Board -> (DecisionNode, Board)
        fillSomeCell curNode b @ (Board _ cs) =
            let
                Just (cell, cellIdx) = boardStateFindUnderdeterminedCell cs        -- Find some underdetermined cell,
                allowedValues        = cellAllowedValues cell                      -- And check which letters are allowed in it.
                (firstAlt:restAlt)   = map (boardSetValue b cellIdx) allowedValues -- Make alternative board snapshots for each letter.
                newNode              = Node [] (withAlternatives restAlt curNode)  -- Use the first one, remember the rest as alternatives.
            in
                (newNode, firstAlt)

        {-|
          Returns a copy of the given node with the list of exclusions updated to the given one.
        -}
        withAlternatives :: [Board] -> DecisionNode -> DecisionNode
        withAlternatives alters (RootNode _) = RootNode alters
        withAlternatives alters (Node _ par) = Node alters par
