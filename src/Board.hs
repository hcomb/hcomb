module Board
    ( Board (..)
    , BoardState
    , boardGetNeighborIndices
    , boardNumCells
    , boardSetValue
    , boardStateFindDeterminedCell
    , boardStateFindUnderdeterminedCell
    , boardStateExclude
    ) where

import Data.List (findIndex, stripPrefix)
import Data.Maybe (listToMaybe)

import Cell

{-|
  Represents current state of the board as a list of cell states (exclusions).

  Rows of the input description are concatenated together, forming a 1D structure,
  as it's easier to deal with flat lists than nested ones.
-}
type BoardState = [CellState]

{-|
  Represents the game board: it's arity (number of elements in the first row) and current state.
-}
data Board = Board Int BoardState

{-|
  Allows for pretty-printing the board.
-}
instance Show Board where
    show board = showBoard board

{-|
  Allows for parsing string representations to boards.
-}
instance Read Board where
    readsPrec _ text = readBoard text

{-|
  Pretty-prints the given board.
-}
showBoard :: Board -> String
showBoard (Board arity cells) =
    "Plaster " ++ (show $ splitIntoRows arity $ showCells cells) where
    
    splitIntoRows :: Int -> String -> [String]
    splitIntoRows n str = doSplit str False where
        doSplit "" _    = []
        doSplit s False = let (row, rest) = splitAt n s       in row : (doSplit rest True)
        doSplit s True  = let (row, rest) = splitAt (n + 1) s in row : (doSplit rest False)
    
    showCells :: BoardState -> String
    showCells [] = ""
    showCells (c:cs) = (showCell c) ++ showCells cs

    showCell :: CellState -> String
    showCell cell =
        if cellClear cell then "."
        else case cellValue cell of
            Just val -> show val
            Nothing  -> "?"

{-|
    Tries to parse a board description into a Board. In case of a successful parse, returns
    a list containing the board and all the trailing text. Otherwise returns an empty list.

    The board description must have the following format:
    Plaster ["BD..", ".GA.D", ".FEG", "ABDCF", "E..."]
    Where:
    1. Each entry represents a single row of the board.
    2. A dot symbol represents an empty field.
    3. The number of symbols (N) in the first entry determines the dimensions of the whole board:
        - Odd rows must contain N symbols,
        - Even rows must contain N + 1 symbols,
        - The whole board must contain N + 1 rows.
-}
readBoard :: String -> [(Board, String)]
readBoard text =
    let maybeParsed =
            do
                noPrefix           <- stripPrefix "Plaster " text
                (rowStrings, rest) <- parseList noPrefix
                parsedRows         <- if (validDimensions rowStrings) then parseRows rowStrings
                                      else                                 Nothing
                let arity           = (length parsedRows) - 1
                let boardState      = foldr (++) [] parsedRows

                Just (Board arity boardState, rest)
    in case maybeParsed of
        Just parsed -> [parsed]
        Nothing     -> []
    where
        {-|
          Tries to parse the given string to a list of string descriptions of row contents.
          The string should have the following format:
          ["BD..", ".GA.D", ".FEG", "ABDCF", "E..."]
        -}
        parseList :: String -> Maybe ([String], String)
        parseList t = listToMaybe (reads t :: [([String], String)])

        {-|
          Verifies that the dimensions of the board are well defined, i.e.:
          1. The number of rows is N + 1,
          2. Odd rows contain N letters,
          3. Even rows contain N + 1 letters,
             where N is the number of letters in the first row.

          If validation fails, returns False. Otherwise returns true.
        -}
        validDimensions :: [String] -> Bool
        validDimensions []           = False
        validDimensions (first:rest) = case length first of
            n | odd n -> False -- The number of letters in the first row MUST be even.
            n         -> (countValid rest n 1) == n + 1
            where
                {-|
                  Recursively validates rows of the board, except the first one.
                  Returns the number of valid rows.
                -}
                countValid [] _ soFar = soFar  -- When there are no more rows to validate, finish.
                countValid (row:rows) arity soFar
                    | length row == (arity + soFar `mod` 2) = countValid rows arity (soFar + 1)
                    | otherwise                             = countValid rows arity soFar

        {-|
          Tries to parse the given row descriptions (e.g. "BD..", ".GA.D") into a list of lists of cell states.
          If parsing fails for any of the rows, returns an empty list.
        -}
        parseRows :: [String] -> Maybe [[CellState]]
        parseRows [] = Just []
        parseRows (h:t) = do
            parsedH <- parseRow h
            parsedT <- parseRows t
            Just (parsedH : parsedT)

        {-|
          Tries to parse a single row description, e.g. "BD..". Returns Nothing on parse error.
        -}
        parseRow :: String -> Maybe [CellState]
        parseRow [] = Just []
        parseRow (h:t) = do
            parsedH <- parseCell h
            parsedT <- parseRow t
            Just (parsedH : parsedT)

        {-
          Tries to parse a single cell description. Letters map to letters, dots to empty cells.
        -}
        parseCell :: Char -> Maybe CellState
        parseCell ch = case reads [ch] :: [(CellValue, String)] of
            [(value, "")] -> Just (cellWithLetter value)
            _ | ch == '.' -> Just emptyCell
            _             -> Nothing

{-|
  Returns a copy of the board with cell at the given index updated to the given value.
-}
boardSetValue :: Board -> Int -> CellValue -> Board
boardSetValue (Board arity cells) idx val =
    Board arity $ (take idx cells) ++ (cellWithLetter val) : (drop (idx + 1) cells)

{-|
  Returns indices of neighbors of a field with the given ID, assuming a board with the given arity.
-}
boardGetNeighborIndices :: Int -> Int -> [Int]
boardGetNeighborIndices arity cellIdx =
    filter (withinBounds arity) [ cellIdx - 1            -- Left,
                                , cellIdx + 1            -- Right,
                                , cellIdx - arity - 1    -- Top left,
                                , cellIdx - arity        -- Top right,
                                , cellIdx + arity        -- Bottom left,
                                , cellIdx + arity + 1  ] -- Bottom right
        where withinBounds a idx = idx >= 0 && idx < boardNumCells a

{-|
  Returns the number of cells in a board of the given arity.
-}
boardNumCells :: Int -> Int
boardNumCells arity = arity * arity + (3 * arity) `div` 2

{-|
  Updates the given board state, excluding the given letter for the given cell.
-}
boardStateExclude :: CellValue -> Int -> BoardState -> BoardState
boardStateExclude val idx cells =
    let
        (prefix, this : suffix) = splitAt idx cells
        updated                 = cellAdd val this
    in
        prefix ++ updated : suffix

{-|
  Looks up a cell satisfying the given predicate within the given board state.
  If a matching cell is found, returns its value and index. Otherwise returns Nothing.
-}
boardStateFindCell :: (CellState -> Bool) -> BoardState -> Maybe (CellState, Int)
boardStateFindCell predicate cells =
    let maybeIdx = findIndex predicate cells
    in
        fmap (\idx -> (cells !! idx, idx)) maybeIdx

{-|
  Looks for an underdetermined cell within the given board state.

  See the generic boardStateFindCell for more information.
-}
boardStateFindUnderdeterminedCell :: BoardState -> Maybe (CellState, Int)
boardStateFindUnderdeterminedCell = boardStateFindCell cellUnderdetermined

{-|
  Looks for a determined cell within the given board state.

  See the generic boardStateFindCell for more information.
-}
boardStateFindDeterminedCell :: BoardState -> Maybe (CellState, Int)
boardStateFindDeterminedCell = boardStateFindCell cellDetermined
