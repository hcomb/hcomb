module Cell
    ( CellValue (..)
    , CellState
    , cellAdd
    , cellAllowedValues
    , cellClear
    , cellDetermined
    , cellHas
    , cellIsAllowed
    , cellOverdetermined
    , cellUnderdetermined
    , cellValue
    , cellWithLetter
    , emptyCell
    ) where

import Data.Bits

{-|
  Represents possible board cell values.

  Integer representations of the values map to bit indices within CellStates.
  So: A - is the youngest, 0th bit. G is the oldest, 7th. Older bits are unused.
-}
data CellValue = A | B | C | D | E | F | G deriving (Enum, Eq, Read, Show)

{-|
  Represents state of a board cell as a set of exclusions. If all letters but one
  are excluded, the a cell is understood to contain the only non-excluded letter.

  If a bit is on, it's corresponding letter is disabled for some board field.

  E.g.: 01100111 means that letters A, B, C, F and G are disallowed, while D and E are not.
-}
type CellState = Int

{-|
  Returns an empty cell (without any exclusions).
-}
emptyCell :: CellState
emptyCell = 0

{-|
  Checks whether the given cell is clear (has no exclusions at all).
-}
cellClear :: CellState -> Bool
cellClear 0 = True 
cellClear _ = False

{-|
  Checks whether the given cell contains a determined letter (has all the other letters excluded).
-}
cellDetermined :: CellState -> Bool
cellDetermined cell = popCount cell == 6

{-|
  Checks whether the given cell is overdetermined, i.e. has ALL the letters excluded.
-}
cellOverdetermined :: CellState -> Bool
cellOverdetermined cell = popCount cell == 7

{-|
  Checks whether the given cell is underdetermined, i.e. has less exclusions than needed to determine
  a particular letter.
-}
cellUnderdetermined :: CellState -> Bool
cellUnderdetermined cell = popCount cell < 6

{-|
  Checks whether the given cell contains the given value, i.e. whether all values
  except for the given one are marked excluded.
-}
cellHas :: CellValue -> CellState -> Bool
cellHas val cell = cellValue cell == Just val

{-|
  Converts the given cell state (set of exclusions) into a cell value, if exclusions
  determine one particular value of the cell. If they do not, returns Nothing.
-}
cellValue :: CellState -> Maybe CellValue
cellValue cell =
    if cellDetermined cell then Just $ toCellValue cell
    else                        Nothing
    where
        toCellValue x | check A = A | check B = B | check C = C
                      | check D = D | check E = E | check F = F
                      | check G = G
            where check val = cellIsAllowed val cell

{-|
  Adds the given letter to the cell's exclusions.
-}
cellAdd :: CellValue -> CellState -> CellState
cellAdd val cell = setBit cell $ fromEnum val

{-|
  Returns a cell state (set of exclusions) corresponding to the given letter being set.
-}
cellWithLetter :: CellValue -> CellState
cellWithLetter val = clearBit allBits (fromEnum val)
    where allBits = 127

{-|
  Returns a list of all non-excluded values for the given cell.
-}
cellAllowedValues :: CellState -> [CellValue]
cellAllowedValues cell =
    let
        listIfAllowed letter = if cellIsAllowed letter cell then [letter] else []
        allowed              = map listIfAllowed [A, B, C, D, E, F, G]
    in foldr (++) [] allowed

{-|
  Checks whether the given value is allowed (non-excluded) for the given cell.
-}
cellIsAllowed :: CellValue -> CellState -> Bool
cellIsAllowed val cell = not $ testBit cell (fromEnum val)
