module Main where

import System.IO

import HComb (Board, solve)

main :: IO ()
main = do
    putStrLn "Podaj nazwę pliku z opisem łamigłówki: "
    fileName <- getLine
    file     <- openFile fileName ReadMode
    contents <- hGetContents file
    let trimmed = trim contents

    putStrLn $ processPuzzle trimmed

    hClose file

    where
        processPuzzle :: String -> String
        processPuzzle boardDesc =
            let
                tryBoard = reads boardDesc :: [(Board, String)]
            in
                case tryBoard of
                    [(board, "")] -> solvePuzzle board
                    _             -> "Nieprawidłowy opis plastra!"
            
        solvePuzzle :: Board -> String
        solvePuzzle board = case solve board of
            Just b  -> show b
            Nothing -> "Zagadka nie posiada rozwiązania!"

        trim :: String -> String
        trim = reverse . dropWhile (== '\n') . reverse